function fish_greeting --wraps='neofetch --source ~/.dotfiles/neofetch/.config/neofetch/gentoo.png' --wraps='neofetch --ascii' --wraps='echo s > /dev/null' --wraps='exa -l' --wraps='exa -la' --wraps='neofetch --source ~/.dotfiles/neofetch/.config/neofetch/logo.png' --description 'alias fish_greeting neofetch --source ~/.dotfiles/neofetch/.config/neofetch/logo.png'
  neofetch --source ~/.dotfiles/neofetch/.config/neofetch/logo.png $argv
        
end
