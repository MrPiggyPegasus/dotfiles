#!/bin/sh

mount=$(cat ~/.dotfiles/waybar/.config/waybar/values/mountpoint)

if [ $mount = "" ]
then
    mount = "/"
fi

warning=20
critical=10

df -h -P -l "$mount" | awk -v warning=$warning -v critical=$critical '
/\/.*/ {
  text=$4
  use=$5
  exit 0
}
END {
  class=""
  gsub(/%$/,"",use)
  if ((100 - use) < critical) {
    class="critical"
  } else if ((100 - use) < warning) {
    class="warning"
  }
  print "{\"text\":\""text"\", \"percentage\":"use", \"class\":\""class"\"}"
}
'
