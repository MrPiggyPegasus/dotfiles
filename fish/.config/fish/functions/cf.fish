function cf --wraps='cargo +nightly fmt --all' --description 'alias cf cargo +nightly fmt --all'
  cargo +nightly fmt --all $argv
        
end
