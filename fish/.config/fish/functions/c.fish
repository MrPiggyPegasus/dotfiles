function c --wraps='clear && exa' --wraps='clear && exa -l' --description 'alias c clear && exa -l'
  clear && exa -l $argv
        
end
