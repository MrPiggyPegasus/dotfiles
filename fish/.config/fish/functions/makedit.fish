function makedit --wraps='EDITOR=nvim sudoedit /etc/portage/make.conf' --description 'alias makedit EDITOR=nvim sudoedit /etc/portage/make.conf'
  EDITOR=nvim sudoedit /etc/portage/make.conf $argv
        
end
