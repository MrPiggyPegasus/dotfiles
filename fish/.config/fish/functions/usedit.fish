function usedit --wraps='EDITOR=nvim sudoedit /etc/portage/package.use' --description 'alias usedit EDITOR=nvim sudoedit /etc/portage/package.use'
  EDITOR=nvim sudoedit /etc/portage/package.use $argv
        
end
