function r --wraps='tput reset' --description 'alias r tput reset'
  tput reset $argv
        
end
