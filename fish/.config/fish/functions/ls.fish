function ls --wraps=exa --wraps='exa -a' --wraps='exa -l' --description 'alias ls exa -l'
  exa -l $argv
        
end
