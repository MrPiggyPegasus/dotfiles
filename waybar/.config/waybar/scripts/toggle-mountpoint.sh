current=$(cat ~/.dotfiles/waybar/.config/waybar/values/mountpoint)

if [ $current = "/" ]
then
    echo "/home" > ~/.dotfiles/waybar/.config/waybar/values/mountpoint
else
    echo "/" > ~/.dotfiles/waybar/.config/waybar/values/mountpoint
fi
